<?php

  class Posts {
    public $post;

    function __construct($post) {
      $this->post = $post;
    }
  }

  /*Serialize - creates posts.txt
  echo "Section 1:<br>";
  $post1 = new Posts("First post");
  $serialized = serialize($post1);
  echo $serialized;
  file_put_contents("posts.txt", $serialized);
   */

  //Unserialize
  $unserialized = file_get_contents("posts.txt");
  $post2 = unserialize($unserialized);
  echo $post2->post;
